<?php

namespace guolei\php\extras\sina;

/***
 * 新浪短连接帮助类
 */
use guolei\php\extras\utils\Util;
use guolei\php\extras\utils\Http;

class ShortLink
{
    /***
     * 获取短连接url
     * @param string $urlLong 需要转换的连接
     * @param string $source 新浪开放平台的app key
     * @return bool|mixed 失败返回false
     */
    public static function getShortLinkUrl($urlLong = '', $source = '')
    {
        $_t = microtime(true);
        $url = "http://api.t.sina.com.cn/short_url/shorten.json?source={$source}&url_long={$urlLong}&_t={$_t}";
        $response = Http::get($url);
        if ($response['responseStatus'] == '200') {
            $responseContent = json_decode($response['responseContent'], true);
            if (is_array($responseContent) && count($responseContent)) {
                return $responseContent[0];
            }
            return false;

        }
        return false;

    }

    /***
     * 批量获取短链url
     * @param array $urlLongs 需要转换的url 数组
     * @param string $source 新浪开放平台app key
     * @return bool|mixed
     */
    public static function getShortLinkUrls($urlLongs = [], $source = '')
    {
        $_t = microtime(true);
        $urls='url_long='.implode('&url_long=',$urlLongs);
        $url = "http://api.t.sina.com.cn/short_url/shorten.json?source={$source}&{$urls}&_t={$_t}";
        $response = Http::get($url);
        if ($response['responseStatus'] == '200') {
            $responseContent = json_decode($response['responseContent'], true);
            if (is_array($responseContent) && count($responseContent)) {
                return $responseContent;
            }
            return false;

        }
        return false;

    }

    /***
     * 获取长连接
     * @param string $urlShort 短连接
     * @param string $source 新浪开放平台 app key
     * @return bool|mixed
     */
    public static function getLongLinkUrl($urlShort = '', $source = '')
    {
        $_t = microtime(true);
        $url = "http://api.t.sina.com.cn/short_url/expand.json?source={$source}&url_short={$urlShort}&_t={$_t}";
        $response = Http::get($url);
        if ($response['responseStatus'] == '200') {
            $responseContent = json_decode($response['responseContent'], true);
            if (is_array($responseContent) && count($responseContent)) {
                return $responseContent[0];
            }
            return false;

        }
        return false;

    }

    /***
     * 批量获取长连接
     * @param array $urlShorts 短链数组
     * @param string $source 新浪开放平台app key
     * @return bool|mixed
     */
    public static function getLongLinkUrls($urlShorts = [], $source = '')
    {
        $_t = microtime(true);
        $urls='url_short='.implode('&url_short=',$urlShorts);
        $url = "http://api.t.sina.com.cn/short_url/expand.json?source={$source}&{$urls}&_t={$_t}";
        $response = Http::get($url);
        if ($response['responseStatus'] == '200') {
            $responseContent = json_decode($response['responseContent'], true);
            if (is_array($responseContent) && count($responseContent)) {
                return $responseContent;
            }
            return false;

        }
        return false;

    }
}