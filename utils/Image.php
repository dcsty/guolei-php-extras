<?php

namespace guolei\php\extras\utils;


class Image
{
    public static function getImageSize($image)
    {
        if (file_exists($image) == false) {
            return false;
        }
        return getimagesize($image);
    }

    public static function toBase64Data($image)
    {
        if (file_exists($image) == false) {
            return false;
        }
        $imageSizeObject = getimagesize($image);
        $base64ImageContent = 'data:{' . $imageSizeObject['mime'] . '};base64,' . chunk_split(base64_encode(file_get_contents($image)));
        return $base64ImageContent;
    }

    public static function base64ToImage($base64, $image)
    {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64, $result)) {
            $type = $result[2];
            $image = $image . '.{$type}';
            if (file_put_contents($image, base64_decode(str_replace($result[1], '', $base64)))) {
                return true;
            }
        }
        return false;
    }

    /***
     * 合成图片 此方法为先渲染背景图 然后渲染二维码
     * @param $outFilePath 合成后图片的url
     * @param $backroundImagePath 背景图片地址
     * @param $imagePath 需要合成的图片地址
     * @param int $hollowX 镂空的x起点
     * @param int $hollowY 镂空的y起点
     * @param int $hollowWidth 镂空的宽度
     * @param int $hollowHeight 镂空的高度
     * @return bool
     * 是否合并成功
     */
    public static function syntheticImage($outFilePath, $backroundImagePath, $imagePath, $hollowX = 0, $hollowY = 0, $hollowWidth = 0, $hollowHeight = 0)
    {
        try {
            //获取背景图片
            $syntheticImage = imagecreatefromstring(file_get_contents($backroundImagePath));

            //获取需要合并的图片
            $image = imagecreatefromstring(file_get_contents($imagePath));
            //获取合并图片的x
            $imageX = imagesx($image);
            //获取合并图片的y
            $imageY = imagesy($image);

            if ($hollowWidth <= 0) {
                $hollowHeight = $imageX;
            }

            if ($hollowHeight <= 0) {
                $hollowHeight = $imageY;
            }

            //合并图片
            imagecopyresampled($syntheticImage, $image, $hollowX, $hollowY, 0, 0, $hollowWidth, $hollowHeight, $imageX, $imageY);

            //输出图片
            imagepng($syntheticImage, $outFilePath);
            return true;
        } catch (\Exception $e) {
            return false;
        }

    }

    /***
     * 合成图片 此方法为先渲染二维码然后覆盖透明图
     * @param $outFilePath 合成后图片的url
     * @param $backroundImagePath 背景图片地址
     * @param $imagePath 需要合成的图片地址
     * @param int $hollowX 镂空的x起点
     * @param int $hollowY 镂空的y起点
     * @param int $hollowWidth 镂空的宽度
     * @param int $hollowHeight 镂空的高度
     * @return bool
     * 是否合并成功
     */
    public static function syntheticImage1($outFilePath, $backroundImagePath, $imagePath, $hollowX = 0, $hollowY = 0, $hollowWidth = 0, $hollowHeight = 0)
    {
        try {

            //背景图片
            $backroundImage = imagecreatefromstring(file_get_contents($backroundImagePath));

            $backroundImageX = imagesx($backroundImage);

            $backroundImageY = imagesy($backroundImage);

            //获取背景图片
            $syntheticImage = imagecreatetruecolor($backroundImageX, $backroundImageY);

            //获取需要合并的图片
            $image = imagecreatefromstring(file_get_contents($imagePath));

            //获取合并图片的x
            $imageX = imagesx($image);
            //获取合并图片的y
            $imageY = imagesy($image);

            if ($hollowWidth <= 0) {
                $hollowHeight = $imageX;
            }

            if ($hollowHeight <= 0) {
                $hollowHeight = $imageY;
            }

            //写入二维码图片
            imagecopyresampled($syntheticImage, $image, $hollowX, $hollowY, 0, 0, $hollowWidth, $hollowHeight, $imageX, $imageY);

            //覆盖背景图片
            imagecopyresampled($syntheticImage, $backroundImage, 0, 0, 0, 0, $backroundImageX, $backroundImageY, $backroundImageX, $backroundImageY);

            //输出图片
            imagepng($syntheticImage, $outFilePath);
            return true;
        } catch (\Exception $e) {
            return false;
        }

    }

    /**
     * 为图片添加文字
     * @param string $imagePath 图片地址
     * @param string $value 添加的文字
     * @param int $fontX 文字x起始位置
     * @param int $fontY 文字y时期未知
     * @param int $angle 文字角度
     * @param int $red 颜色R数值
     * @param int $green 颜色G数值
     * @param int $blue 颜色B数值
     * @param int $fontSize 字体大小
     * @return bool
     */
    public static function writeTextToImage($imagePath = '', $value = '', $fontX = 0, $fontY = 0, $angle = 0, $red = 0, $green = 0, $blue = 0, $fontSize = 20)
    {
        try {
            $image = imagecreatefromstring(file_get_contents($imagePath));
            $font = GUOLEI_PHP_EXTRAS_ROOT_DIR . '/statics/font/msyh.ttf';
            $fontColor = imagecolorallocate($image, $red, $green, $blue);
            imagettftext($image, $fontSize, $angle, $fontX, $fontY, $fontColor, $font, $value);
            imagepng($image, $imagePath);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}