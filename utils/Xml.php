<?php
namespace guolei\php\extras\utils;
class Xml
{
    /***
     * 数组转xml
     * @param $data
     * @return string
     */
    public static function arrayToXml($data)
    {
        $xmlStr = "<xml>";
        $xmlStr .= self::arrayDataToXml($data);
        $xmlStr .= "</xml>";
        return $xmlStr;
    }

    /***
     * 数组数据递归转xml
     * @param $data
     * @return string
     */
    private static function arrayDataToXml($data)
    {
        $xmlStr = "";
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $xmlStr .= "<" . $key . ">" . self::arrayDataToXml($value) . "</" . $key . ">";
            } else {
                if (is_numeric($value)) {
                    $xmlStr .= "<" . $key . ">" . $value . "</" . $key . ">";
                } else {
                    $xmlStr .= "<" . $key . "><![CDATA[" . $value . "]]</" . $key . ">";
                }
            }

        }
        return $xmlStr;
    }

    /***
     * xml转数组
     * @param $xml
     * @return mixed
     */
    public static function xmlToArray($xml)
    {
        libxml_disable_entity_loader(true);
        return json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
    }

    /***
     * xml转json
     * @param $xml
     */
    public static function xmlToJson($xml)
    {
        libxml_disable_entity_loader(true);
        json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA));
    }
}