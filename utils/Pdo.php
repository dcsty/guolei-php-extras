<?php

namespace guolei\php\extras\utils;
class Pdo
{
    private static $dsn = null;

    private static $username = null;

    private static $password = null;

    private static $options = null;

    private static $pdo = null;

    private static $pdoStatement = null;

    /**
     * @return null
     */
    public static function getDsn()
    {
        return self::$dsn;
    }

    /**
     * @return null
     */
    public static function getUsername()
    {
        return self::$username;
    }

    /**
     * @return null
     */
    public static function getPassword()
    {
        return self::$password;
    }

    /**
     * @return null
     */
    public static function getOptions()
    {
        return self::$options;
    }

    /**
     * @return null
     */
    public static function getPdo()
    {
        return self::$pdo;
    }

    /**
     * @return null
     */
    public static function getPdoStatement()
    {
        return self::$pdoStatement;
    }

    /***
     * 打开数据库连接
     * @param $dsn dsn 连接字符串
     * @param $username 用户名
     * @param $password 密码
     * @param array $options 连接选项
     * @return bool
     */
    public static function open($dsn, $username, $password, $options = [
        \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
    ])
    {
        try {
            self::$dsn = $dsn;
            self::$username = $username;
            self::$password = $password;
            self::$options = $options;
            self::$pdo = new \PDO(self::$dsn, self::$username, self::$password, self::$options);
            return true;
        } catch (\Exception $e) {
            die($e);
        }
    }

    /***
     * 通过配置文件打开数据库连接
     * @param string $dbType 数据库类型 默认mysql
     * @param string $name 数据库配置名称 默认master
     * @param string $key 数据库配置配置索引 默认为-1 即随机选择
     * @return bool
     */
    public static function openByConfig($dbType = 'mysql', $name = 'master', $key = '-1')
    {
        try {
            $config = include(GUOLEI_PHP_EXTRAS_ROOT_DIR . '/configs/config.php');
            $runModel = GUOLEI_PHP_EXTRAS_RUN_MODEL;
            $min = 0;
            $max = 0;
            if (intval($key) == -1) {
                $max = count($config['database'][$runModel][$dbType][$name]);
                $key = random_int($min, $max - 1);
            }

            $databseConfig = $config['database'][$runModel][$dbType][$name][$key];
            $dsn = $databseConfig['dsn'];
            $username = $databseConfig['username'];
            $password = $databseConfig['password'];
            $options = $databseConfig['options'];
            return self::open($dsn, $username, $password, $options);
        } catch (\Exception $e) {
            die($e);
        }

    }

    /***
     * 执行sql
     * @param $query sql语句
     * @param array $parameters 参数数组
     * @param bool $identity 是否返回自增id 默认为false
     * @return bool
     */
    public static function execute($query, $parameters = [], $identity = false)
    {
        try {
            self::$pdoStatement = self::$pdo->prepare($query);
            if ($parameters != null && is_array($parameters) && count($parameters)) {
                self::$pdoStatement->execute($parameters);
            } else {
                self::$pdoStatement->execute();
            }
            return $identity ? self::$pdo->lastInsertId() : true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /***
     * 查询返回首条记录
     * @param $query sql
     * @param array $parameters 参数
     * @return array|bool
     */
    public static function findFirst($query, $parameters = [])
    {
        return self::find($query, $parameters, 'one');
    }

    /***
     * 查询返回所有记录
     * @param $query sql
     * @param array $parameters 参数
     * @param string $fetchNums 返回形式 默认为all
     * @return array|bool
     */
    public static function find($query, $parameters = [], $fetchNums = 'all')
    {
        try {
            self::$pdoStatement = self::$pdo->prepare($query);
            if ($parameters != null && is_array($parameters) && count($parameters)) {
                self::$pdoStatement->execute($parameters);
            } else {
                self::$pdoStatement->execute();
            }
            if ($fetchNums == 'one') {
                return self::$pdoStatement->fetch(\PDO::FETCH_BOTH);
            }
            if ($fetchNums == 'one_columns') {
                return self::$pdoStatement->fetch(\PDO::FETCH_ASSOC);
            }
            if ($fetchNums == 'all') {
                return self::$pdoStatement->fetchAll(\PDO::FETCH_BOTH);
            }
            if ($fetchNums == 'all_columns') {
                return self::$pdoStatement->fetchAll(\PDO::FETCH_ASSOC);
            }
            return [];
        } catch (\Exception $e) {
            return false;
        }
    }

    /***
     * 批量执行sql
     * @param array $querys sql
     * @return bool
     */
    public static function batchExecute($querys = [])
    {
        try {
            if (self::$pdo->beginTransaction()) {
                foreach ($querys as $query) {
                    if (is_string($query)) {
                        self::$pdoStatement = self::$pdo->prepare($query);
                        self::$pdoStatement->execute();
                    }
                    if (is_array($query)) {
                        $prepareQuery = isset($query['query']) ? $query['query'] : $query[0];
                        self::$pdoStatement = self::$pdo->prepare($prepareQuery);
                        $parameters = is_set($query['parameters']) ? $query['parameters'] : $query[1];
                        if ($parameters != null && is_array($parameters) && count($parameters)) {
                            self::$pdoStatement->execute($parameters);
                        } else {
                            self::$pdoStatement->execute();
                        }
                    }
                }
                self::$pdo->commit();
                return true;

            }
            return false;
        } catch (\Exception $e) {
            self::$pdo->rollBack();
            return false;
        }
    }

    public static function close()
    {
        if (self::$pdo != null) {
            self::$pdo = null;
        }
        if (self::$pdoStatement != null) {
            self::$pdoStatement = null;
        }
        return true;
    }

    public static function clear()
    {
        self::$dsn = null;
        self::$username = null;
        self::$password = null;
        self::$options = null;
        return self::close();
    }
}