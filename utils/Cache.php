<?php

namespace guolei\php\extras\utils;

use guolei\php\extras\utils\Util;

class Cache
{
    /***
     * 文件缓存配置
     * @var array
     */
    public static $fileCacheConfig = [
        'cacheDir' => GUOLEI_PHP_EXTRAS_ROOT_DIR . '/runtime/caches/files/',
    ];

    /***
     * 设置文件缓存
     * @param string $key key
     * @param string $value value
     * @param int $expire 过期时间 单位：秒
     * @return bool
     */
    public static function setFileCache($key = '', $value = '', $expire = 0)
    {
        try {
            if (strlen($key) == 0) {
                return false;
            }
            if (!is_dir(self::$fileCacheConfig['cacheDir'])) {
                @mkdir(self::$fileCacheConfig['cacheDir'], 0777, true);
            }
            $fileCachePath = self::$fileCacheConfig['cacheDir'] . $key . '.cache';
            if (floatval($expire) > 0) {
                $expire = time() + $expire;
            }
            $fileCacheObj = [
                'fileCacheKey' => $key,
                'fileCacheValue' => $value,
                'fileCacheExpire' => $expire,
            ];
            @file_put_contents($fileCachePath, Util::getJsonStr($fileCacheObj));
            return true;
        } catch (\Exception $e) {
            return false;
        }


    }

    /***
     * 获取文件缓存
     * @param string $key key
     * @return bool|mixed
     */
    public static function getFileCache($key = '')
    {
        try {
            $fileCachePath = self::$fileCacheConfig['cacheDir'] . $key . '.cache';
            if (self::existsFileCache($key)) {
                $fileCacheObj = json_decode(@file_get_contents($fileCachePath), true);
                return $fileCacheObj;
            }
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    /***
     * 删除文件缓存
     * @param string $key key
     * @return bool
     */
    public static function removeFileCache($key = '')
    {
        try {
            $fileCachePath = self::$fileCacheConfig['cacheDir'] . $key . '.cache';
            if (file_exists($fileCachePath)) {
                @unlink($fileCachePath);
            }
            return true;
        } catch (\Exception $e) {
            return false;
        }

    }

    /***
     * 判断文件缓存是否存在
     * @param string $key key
     * @return bool
     */
    public static function existsFileCache($key = '')
    {
        try {
            $fileCachePath = self::$fileCacheConfig['cacheDir'] . $key . '.cache';
            if (file_exists($fileCachePath)) {
                $fileCacheObj = json_decode(@file_get_contents($fileCachePath), true);
                $fileCacheExpire = doubleval($fileCacheObj['fileCacheExpire']);
                if ($fileCacheExpire <= 0) {
                    return true;
                }
                if (time() < $fileCacheExpire) {
                    return true;
                }
                return false;
            } else {
                return false;
            }

        } catch (\Exception $e) {
            return false;
        }

    }


}