<?php

namespace guolei\php\extras\utils;
/***
 * 工具类
 * @author 郭磊
 * @email 174000902@qq.com
 * @phone 15210720528
 * @github https://github.com/guolei19850528
 */
class Util
{
    /***
     * 获取当前访问的ip地址
     * @return mixed
     */
    public static function getIp()
    {
        $ip = 'unknow';
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (!empty($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        } else {
            $ip = 'unknow';
        }
        $ips = explode(',', $ip);
        return $ips[0];
    }

    /***
     * 获取随机字符串
     * @param $length 随机字符串长度
     * @param string $chars 随机字符库
     * @return string
     */
    public static function randomString($length, $chars = '123456789abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ')
    {
        $str = '';
        $max = strlen($chars) - 1;
        for ($i = 0; $i < $length; $i++) {
            $str .= $chars[mt_rand(0, $max)];
        }
        return $str;
    }

    /***
     * 获取随机整数
     * @param int $min 最小值
     * @param $max 最大值
     * @return int
     */
    public static function randomInt($min = 0, $max)
    {
        return mt_rand($min, $max);
    }

    /***
     * 获取uuid
     * @param int $format 格式
     * 0=CD7382CA-4EB3-F44B-97B0-C183FEE6CCF1
     * 1={CD7382CA-4EB3-F44B-97B0-C183FEE6CCF1}
     * 2=(CD7382CA-4EB3-F44B-97B0-C183FEE6CCF1)
     * 3=[CD7382CA-4EB3-F44B-97B0-C183FEE6CCF1]
     * 4=CD7382CA4EB3F44B97B0C183FEE6CCF1
     * @return string
     */
    public static function getUuId($format = 0)
    {
        if (function_exists('com_create_guid')) {
            return com_create_guid();
        } else {
            mt_srand((double)microtime() * 10000);
            //optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);
            $uuid = chr(123) . substr($charid, 0, 8) . $hyphen . substr($charid, 8, 4) . $hyphen . substr($charid, 12, 4) . $hyphen . substr($charid, 16, 4) . $hyphen . substr($charid, 20, 12) . chr(125);
            switch ($format) {
                case '0':
                    $uuid = substr($charid, 0, 8) . $hyphen . substr($charid, 8, 4) . $hyphen . substr($charid, 12, 4) . $hyphen . substr($charid, 16, 4) . $hyphen . substr($charid, 20, 12);
                    break;
                case '1':
                    $uuid = chr(123) . substr($charid, 0, 8) . $hyphen . substr($charid, 8, 4) . $hyphen . substr($charid, 12, 4) . $hyphen . substr($charid, 16, 4) . $hyphen . substr($charid, 20, 12) . chr(125);
                    break;
                case '2':
                    $uuid = '(' . substr($charid, 0, 8) . $hyphen . substr($charid, 8, 4) . $hyphen . substr($charid, 12, 4) . $hyphen . substr($charid, 16, 4) . $hyphen . substr($charid, 20, 12) . ')';
                    break;
                case '3':
                    $uuid = '[' . substr($charid, 0, 8) . $hyphen . substr($charid, 8, 4) . $hyphen . substr($charid, 12, 4) . $hyphen . substr($charid, 16, 4) . $hyphen . substr($charid, 20, 12) . ']';
                    break;
                case '4':
                    $uuid = substr($charid, 0, 8) . substr($charid, 8, 4) . substr($charid, 12, 4) . substr($charid, 16, 4) . substr($charid, 20, 12);
            }
            return $uuid;
        }
    }

    /***
     * 获取文件扩展名
     * @param $fileName
     * @return mixed
     */
    public static function getSuffixName($fileName)
    {
        return pathinfo($fileName, PATHINFO_EXTENSION);
    }

    public static function safeReplace($string)
    {
        $string = str_replace('%20', '', $string);
        $string = str_replace('%27', '', $string);
        $string = str_replace('%2527', '', $string);
        $string = str_replace('*', '', $string);
        $string = str_replace('"', '&quot;', $string);
        $string = str_replace("'", '', $string);
        $string = str_replace('"', '', $string);
        $string = str_replace(';', '', $string);
        $string = str_replace('<', '&lt;', $string);
        $string = str_replace('>', '&gt;', $string);
        $string = str_replace("{", '', $string);
        $string = str_replace('}', '', $string);
        $string = str_replace('\\', '', $string);
        return $string;
    }

    /***
     * 获取当前访问的url
     * @return string
     */
    public static function getUrl()
    {
        $sysProtocal = isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443' ? 'https://' : 'http://';
        $phpSelf = $_SERVER['PHP_SELF'] ? self::SafeReplace($_SERVER['PHP_SELF']) : self::SafeReplace($_SERVER['SCRIPT_NAME']);
        $pathInfo = isset($_SERVER['PATH_INFO']) ? self::SafeReplace($_SERVER['PATH_INFO']) : '';
        $relateUrl = isset($_SERVER['REQUEST_URI']) ? self::SafeReplace($_SERVER['REQUEST_URI']) : $phpSelf . (isset($_SERVER['QUERY_STRING']) ? '?' . self::SafeReplace($_SERVER['QUERY_STRING']) : $pathInfo);
        return $sysProtocal . (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '') . $relateUrl;
    }

    /***
     * 是否微信浏览
     * @param null $userAgent
     * @return false|int
     */
    public static function isWeChatBrower($userAgent = null)
    {
        if ($userAgent == null) {
            $userAgent = $_SERVER['HTTP_USER_AGENT'];
        }
        return preg_match('/MicroMessenger/i', $userAgent);
    }

    /***
     * 是否ipad浏览
     * @param null $userAgent
     * @return false|int
     */
    public static function isIPadBrower($userAgent = null)
    {
        if ($userAgent == null) {
            $userAgent = $_SERVER['HTTP_USER_AGENT'];
        }
        return preg_match('/(iPad).*OS\s([\d_]+)/', $userAgent);
    }

    /***
     * 是否android浏览
     * @param null $userAgent
     * @return false|int
     */
    public static function isAndroidBrower($userAgent = null)
    {
        if ($userAgent == null) {
            $userAgent = $_SERVER['HTTP_USER_AGENT'];
        }
        return preg_match('/(Android)\s+([\d.]+)/', $userAgent);
    }

    /***
     * 是否iphone浏览
     * @param null $userAgent
     * @return bool
     */
    public static function isIPhoneBrower($userAgent = null)
    {
        if ($userAgent == null) {
            $userAgent = $_SERVER['HTTP_USER_AGENT'];
        }
        return !self::isIPadBrower() && preg_match('/(iPhone\sOS)\s([\d_]+)/', $userAgent);
    }

    /***
     * 是否email
     * @param $email
     * @return false|int
     */
    public static function isEmail($email)
    {
        return preg_match('/^[\w\-\.]+@[\w\-\.]+(\.\w+)+$/', $email);
    }

    /***
     * 是否手机号
     * @param $mobile
     * @return false|int
     */
    public static function isMobile($mobile)
    {
        return preg_match('/^(((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/', $mobile);
    }

    /***
     * 递归数组
     * @param int $parentId 父级id
     * @param array $data 需要递归的数组
     * @param string $parentKey 父级key
     * @param string $key 主键key
     * @param string $childKey 子级key
     * @return array
     */
    public static function GetChildren($parentId = 0, $data = [], $parentKey = 'parentId', $key = 'id', $childrenKey = 'children')
    {
        $result = [];
        foreach ($data as $k => $v) {
            if ($v[$parentKey] == $parentId) {
                $temp = $v;
                unset($data[$k]);
                $temp[$childrenKey] = self::GetChildren($temp[$key], $data, $parentKey, $key, $childrenKey);
                $result[] = $temp;
            }
        }
        return $result;
    }

    /***
     * 获取指定天数的秒数
     * @param int $day 天
     * @return float|int
     */
    public static function getDaySeconds($day = 1)
    {
        return (60 * 60 * 24) * intval($day);
    }

    /***
     * 获取json字符串
     * @param array $data
     * @return string
     */
    public static function getJsonStr($data = [])
    {
        return json_encode($data, JSON_UNESCAPED_SLASHES + JSON_UNESCAPED_UNICODE);
    }

    /***
     * 二维数组排序
     * @param $arrays 需要排序的数组
     * @param $sortKey 排序key
     * @param int $sortOrder 排序方式
     * @param int $sortType 排序类型
     * @return bool
     */
    public static function twoDimensionaArraySort($arrays, $sortKey, $sortOrder = SORT_ASC, $sortType = SORT_NUMERIC)
    {
        if (is_array($arrays)) {
            foreach ($arrays as $array) {
                if (is_array($array)) {
                    $keyArrays[] = $array[$sortKey];
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
        array_multisort($keyArrays, $sortOrder, $sortType, $arrays);
        return $arrays;
    }

    public static function byteToKilobyte($byte = 0)
    {
        return doubleval($byte) / 1024;
    }

    public static function kilobyteToMegabyte($kilobyte = 0)
    {
        return doubleval($kilobyte) / 1024;
    }

    public static function byteToMegabyte($byte = 0)
    {
        $kilobyte = self::byteToKilobyte($byte);
        return doubleval($kilobyte) / 1024;
    }

    public static function megabyteToKilobyte($megabyte = 0)
    {
        return doubleval($megabyte) * 1024;
    }

    public static function megabyteTobyte($megabyte = 0)
    {
        $kilobyte = self::megabyteToKilobyte($megabyte);
        return doubleval($kilobyte) * 1024;
    }

    public static function kilobyteTobyte($kilobyte = 0)
    {
        return doubleval($kilobyte) * 1024;
    }

    /**
     * 获取时间戳
     * @param string $format
     * @return false|int
     */
    public static function getTimestamp($format = '')
    {
        if (strlen($format)) {
            return time();
        }
        return strtotime(date($format));
    }

    /**
     * 今天零时时间戳
     * @return false|int
     */
    public static function getTodayTimestamp()
    {
        return self::getTimestamp('Y-m-d');
    }

    /**
     * 获取昨天零时的时间戳
     * @return false|float|int
     */
    public static function getYesterdayTimestamp()
    {
        return $todayTimestamp = self::getTodayTimestamp() - self::getDaySeconds(1);
    }

    /**
     * 获取前一天零时的时间戳
     * @return false|float|int
     */
    public static function getBeforeYesterdayTimestamp()
    {
        return $todayTimestamp = self::getYesterdayTimestamp() - self::getDaySeconds(1);
    }

    /**
     * 获取明天零时的时间戳
     * @return false|float|int
     */
    public static function getTomorrowTimestamp()
    {
        return $todayTimestamp = self::getTodayTimestamp() + self::getDaySeconds(1);
    }

    /**
     * 获取后天零时的时间戳
     * @return false|float|int
     */
    public static function getAfterTomorrowTimestamp()
    {
        return $todayTimestamp = self::getTomorrowTimestamp() + self::getDaySeconds(1);
    }
}