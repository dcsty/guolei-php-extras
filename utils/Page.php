<?php

namespace guolei\php\extras\utils;

/**
 * Class Page
 * 分页处理类
 * @package guolei\php\extras\utils
 */
class Page
{
    //当前页索引
    protected $current = 1;

    //首页索引
    protected $first = 1;

    //最后一页索引
    protected $last = 1;

    //上一页索引
    protected $previous = 1;

    //下一页索引
    protected $next = 1;

    //偏移量
    protected $offset = 0;

    //页数量
    protected $size = 10;

    //总页数
    protected $pages = 1;

    //总记录数
    protected $total = 1;

    //分隔数量
    protected $split = 10;

    //当前分隔页
    protected $currents = [];

    //上一个分隔
    protected $previousSplit = 0;

    //下一个分隔
    protected $nextSplit = 0;

    /**
     * @return int
     */
    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * @param int $current
     */
    public function setCurrent($current)
    {
        if (intval($current) < 1) {
            $current = 1;
        }
        $this->current = $current;
    }

    /**
     * @return int
     */
    public function getFirst()
    {
        $this->first = 1;
        return $this->first;
    }


    /**
     * @return int
     */
    public function getLast()
    {
        $pages = intval($this->getPages());
        $this->last = $pages;
        return $this->last;
    }


    /**
     * @return int
     */
    public function getPrevious()
    {
        $current = intval($this->getCurrent());
        $previous = $current - 1;
        if ($previous < 1) {
            $previous = 1;
        }
        $this->previous = $previous;
        return $this->previous;
    }

    /**
     * @return int
     */
    public function getNext()
    {
        $current = intval($this->getCurrent());
        $pages = intval($this->getPages());
        $next = $current + 1;
        if ($next > $pages) {
            $next = $pages;
        }
        $this->next = $next;
        return $this->next;
    }


    /**
     * @return int
     */
    public function getOffset()
    {
        $current = intval($this->current);
        $size = $this->getSize();
        $offset = ($current - 1) * $size;
        if ($offset < 0) {
            $offset = 0;
        }
        $this->offset = $offset;
        return $this->offset;
    }


    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param int $size
     */
    public function setSize($size)
    {
        if (intval($size) < 1) {
            $size = 10;
        }
        $this->size = $size;
    }

    /**
     * @return int
     */
    public function getPages()
    {
        $pages = 1;
        $total = intval($this->getTotal());
        $size = intval($this->getSize());
        if ($total % $size == 0) {
            $pages = $total / $size;
        } else {
            $pages = intval($total / $size) + 1;
        }
        if ($pages < 1) {
            $pages = 1;
        }
        $this->pages = $pages;
        return $this->pages;
    }


    /**
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal($total)
    {
        if (intval($total) < 0) {
            $total = 0;
        }
        $this->total = $total;
    }

    /**
     * @return int
     */
    public function getSplit()
    {
        return $this->split;
    }

    /**
     * @param int $split
     */
    public function setSplit($split)
    {
        if (intval($split) < 1) {
            $split = 10;
        }
        $this->split = $split;
    }


    /**
     * @return array
     */
    public function getCurrents()
    {
        $split = intval($this->getSplit());
        $current = intval($this->getCurrent());
        $pages = intval($this->getPages());
        if ($current > $pages) {
            $current = $pages;
            $this->setCurrent($pages);
        }
        if ($current < 1) {
            $current = 1;
            $this->setCurrent(1);
        }
        $currents = [];
        if ($current < $split) {
            $start = 1;
            $end = $split+1;
            $this->previousSplit = 0;
            $this->nextSplit = $end + 1;
            if ($pages < $end) {
                $this->nextSplit = 0;
                $end = $pages + 1;
            }
            for ($i = $start; $i < $end; $i++) {
                $currents[] = $i;
            }
        } else {
            $start = intval($current / $split) * $split;
            $end = $start + $split;
            $this->previousSplit = ($start - $split) + 1;
            $this->nextSplit = $end + 1;
            if ($pages < $end) {
                $this->nextSplit = 0;
                $start = ($pages + 1) - $split;
                $end = $pages + 1;

            }
            for ($i = $start; $i < $end; $i++) {
                $currents[] = $i;
            }
        }
        $this->currents = $currents;
        return $this->currents;
    }


    /**
     * @return int
     */
    public function getPreviousSplit()
    {
        return $this->previousSplit;
    }


    /**
     * @return int
     */
    public function getNextSplit()
    {
        return $this->nextSplit;
    }


    public function __construct($total = 1, $size = 10, $current = 1, $split = 10)
    {
        $this->setTotal($total);
        $this->setSize($size);
        $this->setCurrent($current);
        $this->setSplit($split);
        $this->pages = $this->getPages();
        $this->first = $this->getFirst();
        $this->last = $this->getLast();
        $this->previous = $this->getPrevious();
        $this->next = $this->getNext();
        $this->offset = $this->getOffset();
        $this->currents = $this->getCurrents();
        $this->previousSplit = $this->getPreviousSplit();
        $this->nextSplit = $this->getNextSplit();
    }

}