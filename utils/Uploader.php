<?php

namespace guolei\php\extras\utils;
class Uploader
{

    private static $uploadPath = '';

    private static $uploadFileName = '';

    private static $uploadFileType = '';

    private static $uploadFileTmpName = '';

    private static $uploadFileSize = '0';

    private static $uploadFileError = '';

    /**
     * @return string
     */
    public static function getUploadPath()
    {
        return self::$uploadPath;
    }

    /**
     * @param string $uploadPath
     */
    public static function setUploadPath($uploadPath)
    {
        self::$uploadPath = $uploadPath;
    }

    /**
     * @return string
     */
    public static function getUploadFileName()
    {
        return self::$uploadFileName;
    }

    /**
     * @param string $uploadFileName
     */
    public static function setUploadFileName($uploadFileName)
    {
        self::$uploadFileName = $uploadFileName;
    }

    /**
     * @return string
     */
    public static function getUploadFileType()
    {
        return self::$uploadFileType;
    }

    /**
     * @param string $uploadFileType
     */
    public static function setUploadFileType($uploadFileType)
    {
        self::$uploadFileType = $uploadFileType;
    }

    /**
     * @return string
     */
    public static function getUploadFileTmpName()
    {
        return self::$uploadFileTmpName;
    }

    /**
     * @param string $uploadFileTmpName
     */
    public static function setUploadFileTmpName($uploadFileTmpName)
    {
        self::$uploadFileTmpName = $uploadFileTmpName;
    }

    /**
     * @return string
     */
    public static function getUploadFileSize()
    {
        return self::$uploadFileSize;
    }

    /**
     * @param string $uploadFileSize
     */
    public static function setUploadFileSize($uploadFileSize)
    {
        self::$uploadFileSize = $uploadFileSize;
    }

    /**
     * @return string
     */
    public static function getUploadFileError()
    {
        return self::$uploadFileError;
    }

    /**
     * @param string $uploadFileError
     */
    public static function setUploadFileError($uploadFileError)
    {
        self::$uploadFileError = $uploadFileError;
    }

    public static function getSuffixName()
    {
        return pathinfo(self::$uploadFileName, PATHINFO_EXTENSION);
    }

    public static function isLegalSize($min = 0, $max = 0)
    {
        if (doubleval(self::$uploadFileSize) >= doubleval($min) && doubleval(self::$uploadFileSize) <= $max) {
            return true;
        }
        return false;
    }

    public static function isImage()
    {
        switch (self::$uploadFileType) {
            case 'image/jpg':
                return true;
                break;
            case 'image/jpeg':
                return true;
                break;
            case 'image/gif':
                return true;
                break;
            case 'image/png':
                return true;
                break;
            default:
                return false;
                break;
        }
    }


    public static function uploadFile($dir = '', $isAutoMakeDir = true)
    {
        $uploadFileFullPath = self::$uploadPath;

        if (strlen($dir) > 0) {
            $uploadFileFullPath = $uploadFileFullPath . DIRECTORY_SEPARATOR . $dir;
            if (!is_dir($uploadFileFullPath)) {
                @mkdir($uploadFileFullPath);
            }
        }
        if ($isAutoMakeDir) {
            if (!is_dir($uploadFileFullPath)) {
                @mkdir($uploadFileFullPath);
            }

            $uploadFileFullPath = $uploadFileFullPath . DIRECTORY_SEPARATOR . date('Ymd', time());
            if (!is_dir($uploadFileFullPath)) {
                @mkdir($uploadFileFullPath);
            }

            $uploadFileFullPath = $uploadFileFullPath . DIRECTORY_SEPARATOR . date('H', time());
            if (!is_dir($uploadFileFullPath)) {
                @mkdir($uploadFileFullPath);
            }
        }

        $fileNewName = md5(microtime(true)) . '.' . self::getSuffixName();
        $uploadRelativePath = str_replace(self::getUploadPath(), '', $uploadFileFullPath);
        if (@move_uploaded_file(self::$uploadFileTmpName, $uploadFileFullPath . DIRECTORY_SEPARATOR . $fileNewName)) {
            return [
                'uploadFileSuccessfulTime' => time(),
                'uploadFileName' => $fileNewName,
                'uploadFileAbsolutePath' => $uploadFileFullPath . DIRECTORY_SEPARATOR . $fileNewName,
                'uploadRelativePath' => $uploadRelativePath . DIRECTORY_SEPARATOR . $fileNewName,
            ];
        } else {
            return false;
        }

    }


    public static function setUploadFile($name)
    {
        if (is_array($_FILES) && count($_FILES[$name]) > 0) {
            self::$uploadFileName = $_FILES[$name]['name'];
            self::$uploadFileType = $_FILES[$name]['type'];
            self::$uploadFileTmpName = $_FILES[$name]['tmp_name'];
            self::$uploadFileError = $_FILES[$name]['error'];
            self::$uploadFileSize = $_FILES[$name]['size'];
            return true;
        }
        return false;
    }


    public static function clear()
    {
        self::$uploadPath = '';
        self::$uploadFileName = '';
        self::$uploadFileType = '';
        self::$uploadFileTmpName = '';
        self::$uploadFileSize = 0;
        self::$uploadFileError = '';
    }
}