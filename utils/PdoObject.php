<?php
/**
 * Pdo操作类
 */

namespace guolei\php\extras\utils;


class PdoObject
{
    private $dsn = null;

    private $username = null;

    private $password = null;

    private $options = null;

    private $pdo = null;

    private $pdoStatement = null;

    /**
     * @return null
     */
    public function getDsn()
    {
        return $this->dsn;
    }

    /**
     * @return null
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return null
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return null
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @return null
     */
    public function getPdo()
    {
        return $this->pdo;
    }

    /**
     * @return null
     */
    public function getPdoStatement()
    {
        return $this->pdoStatement;
    }

    /***
     * 打开数据库连接
     * @param $dsn dsn 连接字符串
     * @param $username 用户名
     * @param $password 密码
     * @param array $options 连接选项
     * @return bool
     */
    public function open($dsn, $username, $password, $options = [
        \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
    ])
    {
        try {
            $this->dsn = $dsn;
            $this->username = $username;
            $this->password = $password;
            $this->options = $options;
            $this->pdo = new \PDO($this->dsn, $this->username, $this->password, $this->options);
            return true;
        } catch (\Exception $e) {
            die($e);
        }
    }

    /***
     * 通过配置文件打开数据库连接
     * @param string $dbType 数据库类型 默认mysql
     * @param string $name 数据库配置名称 默认master
     * @param string $key 数据库配置配置索引 默认为-1 即随机选择
     * @return bool
     */
    public function openByConfig($dbType = 'mysql', $name = 'master', $key = '-1')
    {
        try {
            $config = include(GUOLEI_PHP_EXTRAS_ROOT_DIR . '/configs/config.php');
            $runModel = GUOLEI_PHP_EXTRAS_RUN_MODEL;
            $min = 0;
            $max = 0;
            if (intval($key) == -1) {
                $max = count($config['database'][$runModel][$dbType][$name]);
                $key = random_int($min, $max - 1);
            }

            $databseConfig = $config['database'][$runModel][$dbType][$name][$key];
            $dsn = $databseConfig['dsn'];
            $username = $databseConfig['username'];
            $password = $databseConfig['password'];
            $options = $databseConfig['options'];
            return $this->open($dsn, $username, $password, $options);
        } catch (\Exception $e) {
            die($e);
        }

    }

    /***
     * 执行sql
     * @param $query sql语句
     * @param array $parameters 参数数组
     * @param bool $identity 是否返回自增id 默认为false
     * @return bool
     */
    public function execute($query, $parameters = [], $identity = false)
    {
        try {
            $this->pdoStatement = $this->pdo->prepare($query);
            if ($parameters != null && is_array($parameters) && count($parameters)) {
                $this->pdoStatement->execute($parameters);
            } else {
                $this->pdoStatement->execute();
            }
            return $identity ? $this->pdo->lastInsertId() : true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /***
     * 查询返回首条记录
     * @param $query sql
     * @param array $parameters 参数
     * @return array|bool
     */
    public function findFirst($query, $parameters = [])
    {
        return $this->find($query, $parameters, 'one');
    }

    /***
     * 查询返回所有记录
     * @param $query sql
     * @param array $parameters 参数
     * @param string $fetchNums 返回形式 默认为all
     * @return array|bool
     */
    public function find($query, $parameters = [], $fetchNums = 'all')
    {
        try {
            $this->pdoStatement = $this->pdo->prepare($query);
            if ($parameters != null && is_array($parameters) && count($parameters)) {
                $this->pdoStatement->execute($parameters);
            } else {
                $this->pdoStatement->execute();
            }
            if ($fetchNums == 'one') {
                return $this->pdoStatement->fetch(\PDO::FETCH_BOTH);
            }
            if ($fetchNums == 'one_columns') {
                return $this->pdoStatement->fetch(\PDO::FETCH_ASSOC);
            }
            if ($fetchNums == 'all') {
                return $this->pdoStatement->fetchAll(\PDO::FETCH_BOTH);
            }
            if ($fetchNums == 'all_columns') {
                return $this->pdoStatement->fetchAll(\PDO::FETCH_ASSOC);
            }
            return [];
        } catch (\Exception $e) {
            return false;
        }
    }

    /***
     * 批量执行sql
     * @param array $querys sql
     * @return bool
     */
    public function batchExecute($querys = [])
    {
        try {
            if ($this->pdo->beginTransaction()) {
                foreach ($querys as $query) {
                    if (is_string($query)) {
                        $this->pdoStatement = $this->pdo->prepare($query);
                        $this->pdoStatement->execute();
                    }
                    if (is_array($query)) {
                        $prepareQuery = isset($query['query']) ? $query['query'] : $query[0];
                        $this->pdoStatement = $this->pdo->prepare($prepareQuery);
                        $parameters = is_set($query['parameters']) ? $query['parameters'] : $query[1];
                        if ($parameters != null && is_array($parameters) && count($parameters)) {
                            $this->pdoStatement->execute($parameters);
                        } else {
                            $this->pdoStatement->execute();
                        }
                    }
                }
                $this->pdo->commit();
                return true;
            }
            return false;
        } catch (\Exception $e) {
            $this->pdo->rollBack();
            return false;
        }
    }

    public function close()
    {
        if ($this->pdo != null) {
            $this->pdo = null;
        }
        if ($this->pdoStatement != null) {
            $this->pdoStatement = null;
        }
        return true;
    }

    public function clear()
    {
        $this->dsn = null;
        $this->username = null;
        $this->password = null;
        $this->options = null;
        return $this->close();
    }


}