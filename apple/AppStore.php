<?php
/***
 * appStore 操作类
 */

namespace guolei\php\extras\apple;

use guolei\php\extras\utils\Util;
use guolei\php\extras\utils\Http;

class AppStore
{
    /***
     * 获取app名称
     * @param int $appStoreId appStoreId
     * @param string $country 国家 cn=中国 us=美国
     * @return string
     */
    public static function getAppName($appStoreId = 0, $country = 'cn')
    {
        if (intval($appStoreId) <= 0) {
            return '';
        }
        if (!in_array($country, ['cn', 'us'])) {
            return '';
        }
        $url = "https://itunes.apple.com/cn/app/id{$appStoreId}?mt=8";
        $headers = [
            'UserAgent:AppStore/2.0 iOS/7.1.2 model/iPhone3,1 build/11D257 (4; dt:27)',
        ];
        if ($country == 'cn') {
            $headers[] = 'X-Apple-Store-Front:143465-19,26 t:native';
        }
        if ($country == 'us') {
            $headers[] = 'X-Apple-Store-Front:143441-1,21 t:native';
        }
        $response = Http::get($url, $headers);
        if ($response['responseStatus'] == '200') {
            $responseContent = json_decode($response['responseContent'], true);
            $appName = $responseContent['storePlatformData']['product-dv-product']['results'][$appStoreId]['name'];
            return $appName;
        } else {
            return '';
        }
    }

    /***
     * 获取app store 排名
     * @param int $appStoreId app store id
     * @param string $searchKeyword 关键词
     * @param string $country 国家 cn=中国 us=美国
     * @return int
     */
    public static function getAppStoreRanking($appStoreId = 0, $searchKeyword = '', $country = 'cn')
    {
        if (intval($appStoreId) <= 0) {
            return 0;
        }
        if (!in_array($country, ['cn', 'us'])) {
            return 0;
        }
        if (strlen($searchKeyword) == 0) {
            return 0;
        }
        $url = "https://itunes.apple.com/WebObjects/MZStore.woa/wa/search?submit=edit&term={$searchKeyword}&media=software&src=hint&clientApplication=Software";
        $clientIp = Util::randomInt(0, 254) . '.' . Util::randomInt(0, 254) . '.' . Util::randomInt(0, 254) . '.' . Util::randomInt(0, 254);
        $headers = [
            'iCloud-DSID: 1463777119',
            'X-Dsid: 1463777119',
            'CLIENT-IP:' . $clientIp,
            'X-FORWARDED-FOR:' . $clientIp,
            'UserAgent: AppStore/2.0 iOS/7.1.2 model/iPhone3,1 build/11D257 (4; dt:27)',
            'Referer: itunes.apple.com',
        ];
        if ($country == 'cn') {
            $headers[] = 'X-Apple-Store-Front:143465-19,26 t:native';
        }
        if ($country == 'us') {
            $headers[] = 'X-Apple-Store-Front:143441-1,21 t:native';
        }
        $response = Http::get($url, $headers);
        if ($response['responseStatus'] == '200') {
            $responseContent = json_decode($response['responseContent'], true);
            $rankings = $responseContent['pageData']['bubbles'][0]['results'];
            $filterRankings = array_column($rankings, 'id');
            $filterRanking = array_keys($filterRankings, $appStoreId);
            if (is_array($filterRanking) && count($filterRanking) > 0) {
                return intval($filterRanking[0]) + 1;
            }
            return 0;

        } else {
            return 0;
        }
    }
}