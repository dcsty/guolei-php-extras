<?php

namespace guolei\php\extras\apple;

use guolei\php\extras\utils\Util;
use guolei\php\extras\utils\Http;

class Push
{
    //推送配置信息
    private static $configs = [
        '0' => [
            'gateway' => 'ssl://gateway.push.apple.com:2195',
//            'gateway' => 'ssl://gateway.sandbox.push.apple.com:2195',//测试推送地址
            'certificate' => [
                'path' => '',
                'passphrase' => '',
            ],
            'apsTemplate' => [
                'alert' => '',
                'sound' => 'default',
            ]
        ],
    ];

    /**
     * @return array
     */
    public static function getConfigs()
    {
        return self::$configs;
    }

    /**
     * @param array $configs
     */
    public static function setConfigs($configs = [])
    {
        self::$configs = $configs;
    }

    public static function chooseConfig($key = 0)
    {
        if (isset(self::$configs[$key]) && is_array(self::$configs[$key]) && count(self::$configs[$key]) > 0) {
            return self::$configs[$key];
        }
        return false;

    }

    /***
     * 通过配置推送
     * @param int $key 配置 key
     * @param string $deviceToken 设备标识
     * @param string $message 信息
     * @param array $apsExtras 推送内容扩展参数
     * @return bool
     */
    public function pushByConfig($key = 0, $deviceToken = '', $message = '', $apsExtras = [])
    {
        try {
            $config = self::chooseConfig($key);
            if (strlen($deviceToken) == 0) {
                return false;
            }
            if (strlen($message) == 0) {
                return false;
            }
            if ($config) {

                $aps = $config['apsTemplate'];
                $aps['alert'] = $message;
                $aps = array_merge($aps, $apsExtras);
                return self::push($deviceToken, $config['gateway'], $aps, $config['certificate']['path'], $config['certificate']['passphrase']);
            }
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    /***
     * 推送
     * @param string $deviceToken 设备标识
     * @param string $gateway 网关
     * @param string $aps 推送内容数组
     * @param string $certificate 推送证书地址
     * @param string $passphrase 推送证书密码
     * @return bool
     */
    public static function push($deviceToken = '', $gateway = '', $aps = [], $certificate = '', $passphrase = '')
    {
        try {
            if (strlen($deviceToken) == 0) {
                return false;
            }
            $streamContext = stream_context_create();
            stream_context_set_option($streamContext, 'ssl', 'local_cert', $certificate);
            stream_context_set_option($streamContext, 'ssl', 'passphrase', $passphrase);
            $payload = json_encode(['aps' => $aps]);
            $messageBinaryStr = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
            $socketClient = stream_socket_client($gateway, $errno, $errstr, 60, STREAM_CLIENT_CONNECT, $streamContext);
            if ($socketClient) {
                $result = fwrite($socketClient, $messageBinaryStr, strlen($messageBinaryStr));
                if (!$result) {
                    return false;
                }
                return true;
            }
            return false;

        } catch (\Exception $e) {
            return false;
        }
    }
}