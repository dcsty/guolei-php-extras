<?php
/**
 * 项目主文件 使用时需要 include 此文件
 * @author 郭磊
 * @email 174000902@qq.com
 * @phone 15210720528
 * @github https://github.com/guolei19850528
 */

//设置当前时区
ini_set('date.timezone', 'Asia/Shanghai');

//设置错误消息显示级别
ini_set('display_errors', '1');

//设置内存大小
//ini_set('memory_limit', -1);

//设置超时时间
//set_time_limit(0);

//设置根目录
defined('GUOLEI_PHP_EXTRAS_ROOT_DIR') or define('GUOLEI_PHP_EXTRAS_ROOT_DIR', __DIR__);

//设置运行模式
defined('GUOLEI_PHP_EXTRAS_RUN_MODEL') or define('GUOLEI_PHP_EXTRAS_RUN_MODEL', 'developer');

include_once(GUOLEI_PHP_EXTRAS_ROOT_DIR . '/utils/Util.php');
include_once(GUOLEI_PHP_EXTRAS_ROOT_DIR . '/utils/Http.php');
include_once(GUOLEI_PHP_EXTRAS_ROOT_DIR . '/utils/Xml.php');
include_once(GUOLEI_PHP_EXTRAS_ROOT_DIR . '/utils/Pdo.php');
include_once(GUOLEI_PHP_EXTRAS_ROOT_DIR . '/utils/PdoObject.php');
include_once(GUOLEI_PHP_EXTRAS_ROOT_DIR . '/utils/Redis.php');
include_once(GUOLEI_PHP_EXTRAS_ROOT_DIR . '/utils/Mail.php');
include_once(GUOLEI_PHP_EXTRAS_ROOT_DIR . '/utils/Cache.php');
include_once(GUOLEI_PHP_EXTRAS_ROOT_DIR . '/utils/Uploader.php');
include_once(GUOLEI_PHP_EXTRAS_ROOT_DIR . '/utils/Image.php');
include_once(GUOLEI_PHP_EXTRAS_ROOT_DIR . '/utils/ImageVertifyCode.php');
include_once(GUOLEI_PHP_EXTRAS_ROOT_DIR . '/utils/Page.php');
include_once(GUOLEI_PHP_EXTRAS_ROOT_DIR . '/alibaba/Taobao.php');
include_once(GUOLEI_PHP_EXTRAS_ROOT_DIR . '/apple/AppStore.php');
include_once(GUOLEI_PHP_EXTRAS_ROOT_DIR . '/apple/Push.php');
include_once(GUOLEI_PHP_EXTRAS_ROOT_DIR . '/sina/ShortLink.php');