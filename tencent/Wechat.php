<?php

namespace guolei\php\extras\tencent;

use guolei\php\extras\utils\Util;
use guolei\php\extras\utils\Http;
use guolei\php\extras\utils\Cache;

class Wechat
{
    private $appId = "";
    private $appSecret = "";
    private $payKey = "";
    private $mchId = "";
    private $payNotifyUrl = "";
    private $timeOut = 7200;

    public function __construct()
    {
    }

    public function choose($key = '0')
    {
        $config = require(GUOLEI_PHP_EXTRAS_ROOT_DIR . "/configs/config.php");
        $extarsConfig = $config['extras'];
        $wechatConfig = $extarsConfig["wechat"][$key];
        $this->appId = $wechatConfig["appId"];
        $this->appSecret = $wechatConfig["appSecret"];
        $this->payKey = $wechatConfig["payKey"];
        $this->mchId = $wechatConfig["mchId"];
        $this->payNotifyUrl = $wechatConfig["payNotifyUrl"];
        $this->timeOut = $wechatConfig["timeOut"];

    }

    public function getAccessToken()
    {
        $fileCacheKey = "wechatAccess_" . $this->appId;
        if (Cache::existsFileCache($fileCacheKey) == false) {
            return Cache::getFileCache($fileCacheKey);
        }
        $requestUrl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" . $this->appId . "&secret=" . $this->appSecret;

        $httpResult = Http::get($requestUrl);
        $httpContent = json_decode($httpResult["content"], true);
        $accessToken = $httpContent["access_token"];
        //此处需要缓存
        Cache::setFileCache($fileCacheKey, $accessToken, $this->timeOut);
        return Cache::getFileCache($fileCacheKey);
    }

    public function getJsApiTicket()
    {
        $fileCacheKey = "wechatJsApiTicket_" . $this->appId;
        if (Cache::existsFileCache($fileCacheKey) == false) {
            return Cache::getFileCache($fileCacheKey);
        }
        $accessToken = $this->GetAccessToken();
        $requestUrl = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=" . $accessToken . "&type=jsapi";
        $httpResult = Http::get($requestUrl);
        $httpContent = json_decode($httpResult["content"], true);
        $ticket = $httpContent["ticket"];
        //此处需要缓存
        Cache::setFileCache($fileCacheKey, $ticket, $this->timeOut);
        return Cache::getFileCache($fileCacheKey);
    }

    public static function getSignature($url = '', $signType = 'shar1')
    {
        $jsApiTicket = self::getJsApiTicket();
        $nonceStr = Util::randomString(16) . time();
        $timestamp = time();
        $string1 = 'jsapi_ticket=' . $jsApiTicket . '&noncestr=' . $nonceStr . '&timestamp=' . $timestamp . '&url=' . $url;
        $signature = sha1($string1);
        if ($signType == 'md5') {
            $signature = md5($string1);
        }
        return [
            'nonceStr' => $nonceStr,
            'signature' => $signature,
            'timestamp' => $timestamp,
        ];
    }

    public function getCodeUrl($url)
    {
        $submitUrl = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" . $this->appId . "&redirect_uri=" . urlencode($url) . "&response_type=code&scope=snsapi_base&state=1#wechat_redirect";
        return $submitUrl;
    }

    public function getOpenId($code)
    {
        $requestUrl = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=" . $this->appId . "&secret=" . $this->appSecret . "&code=" . $code . "&grant_type=authorization_code";
        $response = Http::get($requestUrl);
        $responseContent = json_decode($response["responseContent"], true);
        $openId = $responseContent["openid"];
        return $openId;

    }
}