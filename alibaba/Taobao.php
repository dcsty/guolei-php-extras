<?php
namespace guolei\php\extras\alibaba;
use guolei\php\extras\utils\Util;
use guolei\php\extras\utils\Http;
class Taobao
{
    /***
     * 获取ip地址信息
     * @param string $ip ip地址 ''为当前ip地址
     * @return bool|mixed
     */
    public static function getIpData($ip=''){
        if(strlen($ip)==0){
            $ip=Util::getIp();
        }
        $_t=microtime(true);
        $url="http://ip.taobao.com/service/getIpInfo.php?ip={$ip}&_t={$_t}";
        $response=Http::get($url);
        $responseStatus=$response['responseStatus'];
        if($responseStatus=='200'){
            $responseContent=$response['responseContent'];
            return json_decode($responseContent,true);
        }
        return false;
    }
}